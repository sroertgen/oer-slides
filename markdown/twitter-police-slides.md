---
author: Dr. Sven Bingert & Steffen Rörtgen
title: Data Science Made Easy - Hasskommentare mit AI identifizieren
date: 19.06.2019
revealjs-url: 'https://revealjs.com '

slideNumber: true
---

[//]: # (USAGE: pandoc -t revealjs -s -o test.html test.md -V revealjs-url=./reveal.js)

## Lizenzinformationen

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Hasskommentare mit KI identifizieren</span> von <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Sven Bingert & Steffen Rörtgen</span> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.

## Herzlich willkommen


## Vorstellungsrunde

Wer bist du?

Bist Du schon einmal mit Data Science in Berührung gekommen?

![](images/vorstellungsrunde.png){ width=20% }

<section data-notes="Habt ihr schonmal im Unterricht darüber gesprochen?"></section>



## Data Science und Big Data kurz erklärt
<iframe width="560" height="315" src="https://www.youtube.com/embed/uH813u7_b0s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Was ist Data Science und wo begegnet sie euch?
Hier sollten einige Notizen stehen

::: notes

Hier sind meine Notizen

:::


## Nochmal zwei Bilder nebeneinander

:::::::::::::: {.columns}
::: {.column width="50%"}
![](images/vorstellungsrunde.png){ width=50% }
:::
::: {.column width="50%"}
![](images/vorstellungsrunde.png){ width=50% }
:::
::::::::::::::


## Hasskommentare
<span class="fragment">Wie identifiziert ihr Hasskommentare?</span>
<br>
<span class="fragment">Wie könnte uns Data Science bei der Identifikation helfen?</span>


## Beispiel Tweet
<blockquote class="twitter-tweet" data-lang="de"><p lang="de" dir="ltr">Ich finde politisches Engagement von Schülerinnen und Schülern toll. Von Kindern und Jugendlichen kann man aber nicht erwarten, dass sie bereits alle globalen Zusammenhänge, das technisch Sinnvolle und das ökonomisch Machbare sehen. Das ist eine Sache für Profis. CL</p>&mdash; Christian Lindner (@c_lindner) <a href="https://twitter.com/c_lindner/status/1104683096107114497?ref_src=twsrc%5Etfw">10. März 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

- Arbeitet in Partnerarbeit
    - eine Gruppe schreibt Hasskommentare
    - eine Gruppe schreibt neutrale / positive Kommentare


## Tragt eure Ergebnisse zusammen
- <https://kurzelinks.de/twitter-polizei-negativ>
- <https://kurzelinks.de/twitter-polizei-neutral>


## Und jetzt trainieren wir unseren Hass-Detektor
Schauen wir einmal in den Datensatz:
- <https://kurzelinks.de/twitter-polizei-data>


- Und jetzt öffnet Orange3


## Zusammenfassung


## Vielen Dank für eure Aufmerksamkeit
<iframe src="https://giphy.com/embed/xIJLgO6rizUJi" width="480" height="367" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
