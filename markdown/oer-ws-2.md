---
author: Axel Klinger
title: GitLab Workshop II - OERcamp Lübeck 2019
date: 13./14. Juni 2019
revealjs-url: 'reveal.js'
theme: isovera
css:
  - 'https://fonts.googleapis.com/css?family=Roboto+Slab:700'
---

## Inhalt

* Abbildung einer Schule
* Abbildung eines Studiengangs

# Studiengang

## Darstellung

* einfaches Projekt mir README
* Fächer und Teilfächer
* Sicht des Studierenden
* Sicht des Lehrenden
* Weitere Sichten?

## Verteilte Rollen

- Pflege des Gesamtauftritts
- Bearbeitung der Kurse
- Zuarbeiten der Inhalte

# Schule

## Darstellung

* Klassen und Fächer
* Sicht für Schüler
* Sicht für Lehrer
* Sicht für Planer

# Weitere Themen

## Nächste Schritte

* Generieren von Ansichten HTML, PDF, EPUB
* Merge-Konflikte lösen
* Offline arbeiten

## Fragen?

* axel.klinger@tib.eu
