# My Slide Decks

based on [Benji's Slide Decks](https://gitlab.com/benjifisher/slide-decks)

## Live demo

See the generated presentations on my
[GitLab Pages](https://s.roertgen.gitlab.io/oer-slides/index.html).


## Tips / Cheatsheet
[GitLab-Markdown Guide](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)


### Lizeninformationen einfügen

Auf die [Internetseite von Creative Commons](https://creativecommons.org/choose/#metadata)
gehen und dort die entsprechenden Felder ausfüllen. Den generierten Link dann am 
Anfang der Präsentation einfügen.


### Shortcuts
- "S" öffnet ein neues Fenster für eine Referentenansicht
- "B" pausiert die Präsentation und schaltet den Bildschirm dunkel
- "ESC" öffnet eine Übersicht aller Slides


### "Animationen" in Slides einfügen

Inhalte sollen nacheinander eingeblendet werden? Kein Problem.

```html
<span class="fragment">This get's first displayed</span> <span class="fragment">This second</span> <span class="fragment">this third.</span>
```

wird ein `<br>` zwischen den `<span>`-Blöcken eingefügt, lässt sich Inhalt nacheinander 
wie in einer Liste anzeigen

```html
<span class="fragment">Wie identifiziert ihr Hasskommentare?</span> 
<br>
<span class="fragment">Wie könnte uns Data Science bei der Identifikation helfen?</span>
```


### Bilder einfügen

Grundsätzlich ist in dem oben verlinkten Markdown Guide bereits beschrieben, wie 
Bilder eingefügt werden können. An dieser Stelle noch der Hinweis: 

**IN GITLAB DÜRFEN NUR SCREENSHOTS UND PUBLIC DOMAIN BILDER HOCHGELADEN WERDEN**

Die Größe der Bilder lässt sich wie folgt anpassen:

![Beschreibung](path/to/image.png){ width=50% }

[s. Pandoc-Dokumentation](https://pandoc.org/MANUAL.html#images)


### Inhalte in zwei Spalten

:::::::::::::: {.columns}
::: {.column width="50%"}
![](images/vorstellungsrunde.png){ width=50% }
:::
::: {.column width="50%"}
![](images/vorstellungsrunde.png){ width=50% }
:::
::::::::::::::

### Vortragsnotizen

Notizen, die nur in der "Speaker"-Ansicht sichtbar sind, lassen sich ebenfalls 
leicht einfügen:

```html
<section data-notes="Deine Notiz"></section>
```


## Quick Start

```
git clone --recurse-submodules git@gitlab.com:s.roertgen/oer-slides.git
cd slide-decks
make
```

This will create `html/example.html` from the markdown source
`markdown/example.md`.

## Make targets

You can also `make FILE.html` when `markdown/FILE.md` is any available source
file. The output will be created in the `html/` directory.

The command `make build` will create HTML files in the `html/` directory for
all sources `markdown/*.md`.

## Version of Reveal.js

This repository includes Isovera'a fork of
[reveal.js](https://github.com/isovera/reveal.js)
as a `git` submodule.
You may prefer to start with the original.
If you do, then you will have to adjust the theme reference in the existing
presentations.


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Benji's Slide Decks</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/benjifisher/slide-decks" property="cc:attributionName" rel="cc:attributionURL">Benji Fisher</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
